<%@ page import="java.util.List" %>
<%@ page import="db.MysqlController" %>
<%@ page import="java.sql.SQLException" %>
<%--
  Created by IntelliJ IDEA.
  User: JCD
  Date: 16.04.2015
  Time: 2:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    MysqlController mysql = new MysqlController();
    String username = mysql.getUsernameIfLogged(session.getId());
    boolean logged = username != null;
    String alert = null;
    if (logged && "POST".equals(request.getMethod().toUpperCase())) {
        if (request.getParameter("cardNumberMinus") != null) {
            String cardNumber = request.getParameter("cardNumberMinus");
            Double summ = Double.valueOf(request.getParameter("summ")) * -100;
            try {
                if (mysql.accountExists(cardNumber)) {
                    mysql.changeAccount(cardNumber, summ);
                    alert = "Операция успешно произведена";
                } else {
                    alert = "Такого счёта не существует";
                    response.sendError(418);
                }
            } catch (SQLException e) {
                alert = "Не удалоссь произвести вычет";
                response.sendError(418);
            }
        } else if (request.getParameter("cardNumberPlus") != null) {
            Double summ = Double.valueOf(request.getParameter("summ")) * 100;
            String cardNumber = request.getParameter("cardNumberPlus");
            try {
                if (mysql.accountExists(cardNumber)) {
                    mysql.changeAccount(cardNumber, summ);
                    alert = "Операция успешно произведена";
                } else {
                    alert = "Такого счёта не существует";
                    response.sendError(418);
                }
            } catch (SQLException e) {
                alert = "Не удалоссь произвести пополнение";
                response.sendError(418);
            }
        } else if (request.getParameter("owner") != null) {
            String owner = request.getParameter("owner");
            String recipient = request.getParameter("recipient");
            Double summ = Double.valueOf(request.getParameter("summ")) * 100;
            try {
                mysql.transfer(owner, recipient, summ);
                alert = "Операция успешно произведена";
                response.sendError(418);
            } catch (SQLException e) {
                alert = "Не удалось произвести перевод";
                response.sendError(418);
            }
        }
    }
%>
<jsp:include page="header.jsp"/>
<script>
    <%=alert != null ? "alert('" + alert + "')" : ""%>;
</script>
<div class="container">

<h3>Управлять денежными средствами:</h3>
<p>Вычитать из баланса указанную сумму с указанного банковского счета</p>
<p>Добавлять указанную сумму на указанный банковский счет</p>
<p>Переводить указанную сумму с одного банковского счета на другой</p>

<form class="form-inline" method="post">
    Номер карты: <input type="text" name="cardNumberMinus"/>
    Сумма: <input type="text" name="summ"/>
    <input class="btn" type="submit" value="Вычесть"/>
</form>
<form class="form-inline" method="post">
    Номер карты: <input type="text" name="cardNumberPlus"/>
    Сумма: <input type="text" name="summ"/>
    <input class="btn" type="submit" value="Добавить"/>
</form>
<form class="form-inline" method="post">
    Номер карты клиента: <input type="text" name="owner"/>
    Номер карты получателя: <input type="text" name="recipient"/>
    Сумма: <input type="text" name="summ"/>
    <input class="btn" type="submit" value="Перевести"/>
</form>
</div>
<jsp:include page="footer.jsp"/>
