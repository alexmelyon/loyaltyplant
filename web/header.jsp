<%@ page import="db.MysqlController" %>
<%--
  Created by IntelliJ IDEA.
  User: JCD
  Date: 20.04.2015
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    MysqlController mysql = new MysqlController();
    // TODO: check login
    String username = mysql.getUsernameIfLogged(session.getId());
    boolean logged = username != null;
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Loyaltyplant</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 60px;
            padding-bottom: 40px;
        }
    </style>
    <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="../assets/ico/favicon.png">
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="brand" href="/test.jsp">Loyaltyplant</a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active"><a href="/test.jsp">Home</a></li>
                    <li><a href="/users.jsp">Пользователи</a></li>
                    <li><a href="/accounts.jsp">Счета</a></li>
                </ul>
                <%
                    if (logged) {
                        // update last entry
                        mysql.updateLastViewed(username);
                %>
                <form class="navbar-form pull-right" action="/logout.jsp" method="post">
                    <input type="submit" class="btn" value="Выйти"/>
                </form>
                <p class="navbar-text pull-right">
                    <a href="#" class="navbar-link"><%=username%></a>&nbsp;&nbsp;&nbsp;
                </p>
                <%
                } else {
                %>
                <form class="navbar-form pull-right" action="/login.jsp" method="post">
                    <input class="span2" type="text" placeholder="Имя" name="username">
                    <input class="span2" type="password" placeholder="Пароль" name="password">
                    <button type="submit" class="btn">Войти</button>
                </form>
                <%
                    }
                %>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
