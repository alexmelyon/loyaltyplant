<%@ page import="java.util.List" %>
<%@ page import="db.MysqlController" %>
<%@ page import="beans.User" %>
<%@ page import="java.util.Random" %>
<%--
  Created by IntelliJ IDEA.
  User: JCD
  Date: 16.04.2015
  Time: 2:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"/>
<%
    MysqlController mysql = new MysqlController();
    String username = mysql.getUsernameIfLogged(session.getId());
    boolean logged = username != null;
    if (logged && "POST".equals(request.getMethod().toUpperCase())) {
        if (request.getParameter("delete") != null) {
            mysql.deleteUser(request.getParameter("delete"));
            response.sendRedirect(request.getRequestURI());
        } else if (request.getParameter("user") != null && request.getParameter("pass") != null) {
            StringBuilder cardNumber = new StringBuilder();
            Random rand = new Random();
            cardNumber.append(String.format("%04d", rand.nextInt(10000)) + " ");
            cardNumber.append(String.format("%04d", rand.nextInt(10000)) + " ");
            cardNumber.append(String.format("%04d", rand.nextInt(10000)) + " ");
            cardNumber.append(String.format("%04d", rand.nextInt(10000)));
            mysql.createUser(request.getParameter("user"), request.getParameter("pass"), cardNumber.toString());
            response.sendRedirect(request.getRequestURI());
        } else if (request.getParameter("getCardnumber") != null) {
            String cardNumber = mysql.getCardnumberBySessionId(session.getId());
            out.write(cardNumber);
            return;
        } else if (request.getParameter("getRandomCardnumber") != null) {
//            String cardNumber = mysql.getRandomCardnumber(session.getId());
//            out.write(cardNumber);
            out.write("1383 3230 3707 7826");
            return;
        }
    }
%>
<div class="container">
    <h3>Управление банковскими счетами:</h3>

    <p>Добавлять/удалять банковские счета (пользовательские аккаунты)</p>

    <p>Получать список всех существующих банковских счетов</p>

    <p>Получать остаток средств на указанном банковском счете</p>

    <%if (logged) {%>
    <form method="post" class="form-inline">
        <input style="display:none">
        <input type="password" style="display:none">

        <label class="sr-only" for="user">Имя пользователя:</label>
        <input id="user" class="form-control" type="text" name="user" value=""/>
        <label class="sr-only" for="pass">Пароль:</label>
        <input id="pass" type="password" name="pass" value=""/>
        <input class="btn" type="submit" value="Добавить"/>
    </form>
    <%}%>
    <table class="table table-hover">
        <tr>
            <td>#</td>
            <td>Имя пользователя</td>
            <td>Номер карты</td>
            <td>Остаток</td>
            <td>&nbsp;</td>
        </tr>
        <%
            List<User> users = mysql.getAllUsersLimited(0, 0);
            for (int i = 0; i < users.size(); i++) {
                User user = users.get(i);
        %>
        <tr class="<%=i%2==0 ? "active": ""%>">
            <td><%=i%>
            </td>
            <td><%=user.username%>
            </td>
            <td><%=user.cardNumber%>
            </td>
            <td>
                    <%=user.getAccount()%>
            <td/>
            <td>
                <%if (logged && !"admin".equals(user.username)) {%>
                <form method="post">
                    <input type="hidden" name="delete" value="<%=user.username%>"/>
                    <input type="submit" value="X" onclick="return confirm('Удалить пользователя <%=user.username%>?')"/>
                </form>
                <%}%>
            </td>
        </tr>
        <%
            }
        %>
    </table>
</div>
<jsp:include page="footer.jsp"/>
