<%@ page import="db.MysqlController" %>
<%--
  Created by IntelliJ IDEA.
  User: JCD
  Date: 15.04.2015
  Time: 7:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if ("POST".equals(request.getMethod().toUpperCase())) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String hash = MysqlController.encryptPassword(password);
        new MysqlController().logIn(username, hash, session.getId());
        response.sendRedirect("http://" + request.getHeader("Host"));

    }
%>