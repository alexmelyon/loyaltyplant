<%@ page import="db.MysqlController" %>
<%--
  Created by IntelliJ IDEA.
  User: JCD
  Date: 15.04.2015
  Time: 7:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    MysqlController mysql = new MysqlController();
    String username = mysql.getUsernameIfLogged(session.getId());
    mysql.setSession(username, "");
    response.sendRedirect("http://" + request.getHeader("Host"));
%>