package beans;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by JCD on 16.04.2015.
 */
public class User {
    public String username;
    public String cardNumber;
    public BigDecimal account;

    public User(String name, String cardNumber, BigDecimal account) {
        this.username = name;
        this.cardNumber = cardNumber;
        this.account = account;
    }

    public String getAccount() {
        BigDecimal res = this.account.setScale(2).divide(new BigDecimal(100));
        return res.toString();
    }
}
