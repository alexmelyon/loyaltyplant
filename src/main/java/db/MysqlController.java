package db;

import beans.User;
import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by JCD on 15.04.2015.
 */
public class MysqlController {
    static Logger logger = Logger.getLogger(MysqlController.class);

    static ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/loyaltyplant";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "";

    public MysqlController() {

        try {
            Class.forName(JDBC_DRIVER);
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    public List<User> getAllUsersLimited(int start, int count) throws SQLException {
        ResultSet rs = null;
        Statement stmt = null;
        Connection conn = null;
        try {


            conn = DriverManager.getConnection(DB_URL, USER, PASS);


            stmt = conn.createStatement();
            String sql;
            sql = "SELECT username, cardnumber, account FROM users" + (count > 0 ? (" LIMIT " + start + ", " + count) : "");
            lock.readLock().lock();
            rs = stmt.executeQuery(sql);
            lock.readLock().unlock();

            ArrayList<User> users = new ArrayList<User>();
            while (rs.next()) {
                //Retrieve by column name
                String name = rs.getString("username");
                String cardNumber = rs.getString("cardnumber");
                BigDecimal account = rs.getBigDecimal("account");
                users.add(new User(name, cardNumber, account));
            }
            return users;
        } finally {
            closeAll(rs, stmt, conn);
        }
    }

    void closeAll(ResultSet rs, Statement stmt, Connection conn) throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (stmt != null) {
            stmt.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public String getUsernameIfLogged(String jsessionid) throws SQLException {
        ResultSet rs = null;
        Statement stmt = null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            stmt = conn.createStatement();
            String sql;
            sql = "SELECT username, logged FROM users WHERE session='" + jsessionid + "' AND logged > NOW() - INTERVAL 120 MINUTE";
            lock.readLock().lock();
            rs = stmt.executeQuery(sql);
            lock.readLock().unlock();
            if (rs.next()) {
                return rs.getString("username");
            }
            return null;
        } finally {
            closeAll(rs, stmt, conn);
        }
    }

    public void updateLastViewed(String username) throws SQLException {
        ResultSet rs = null;
        Statement stmt = null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            stmt = conn.createStatement();
            String sql;
            sql = "UPDATE users SET logged=CURRENT_TIMESTAMP WHERE username='" + username + "'";
            lock.writeLock().lock();
            stmt.executeUpdate(sql);
            lock.writeLock().unlock();
        } finally {
            closeAll(rs, stmt, conn);
        }
    }

    public boolean setSession(String username, String jsessionid) throws SQLException {
        ResultSet rs = null;
        Statement stmt = null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            stmt = conn.createStatement();
            String sql;
            sql = "UPDATE users SET session='" + jsessionid + "' WHERE username='" + username + "'";
            lock.writeLock().lock();
            stmt.executeUpdate(sql);
            lock.writeLock().unlock();
            return true;
        } finally {
            closeAll(rs, stmt, conn);
        }
    }

    public static String encryptPassword(String password) {
        String sha1 = "";
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(password.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return sha1;
    }

    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    public boolean logIn(String username, String passHash, String jsessionid) throws SQLException {
        User user = getUser(username, passHash);
        if (user.username != null) {
            setSession(username, jsessionid);
            updateLastViewed(username);
            return true;
        }
        return false;
    }

    public User getUser(String username, String passHash) throws SQLException {
        ResultSet rs = null;
        Statement stmt = null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            stmt = conn.createStatement();
            String sql;
            sql = "SELECT username, cardnumber, account FROM users WHERE username='" + username + "' AND passhash='" + passHash + "'";
            lock.readLock().lock();
            rs = stmt.executeQuery(sql);
            lock.readLock().unlock();
            if (rs.next()) {
                String name = rs.getString("username");
                String cardnumber = rs.getString("cardnumber");
                BigDecimal account = rs.getBigDecimal("account");
                return new User(name, cardnumber, account);
            }
            return null;
        } finally {
            closeAll(rs, stmt, conn);
        }
    }

    public void deleteUser(String username) throws SQLException {
        ResultSet rs = null;
        Statement stmt = null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "DELETE FROM users WHERE username='" + username + "'";
            lock.writeLock().lock();
            stmt.executeUpdate(sql);
            lock.writeLock().unlock();
        } finally {
            closeAll(rs, stmt, conn);
        }
    }

    public boolean createUser(String username, String password, String cardNumber) throws SQLException {
        String passHash = encryptPassword(password);
        //
        ResultSet rs = null;
        Statement stmt = null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "INSERT INTO users(`username`, `passhash`, `cardnumber`) VALUES ('" + username + "', '" + passHash + "', '" + cardNumber + "')";
            lock.writeLock().lock();
            stmt.executeUpdate(sql);
            lock.writeLock().unlock();
            return true;
        } finally {
            closeAll(rs, stmt, conn);
        }
    }

    public void changeAccount(String cardNumber, Double summ) throws SQLException {

        ResultSet rs = null;
        Statement stmt = null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "UPDATE users SET account=account+'" + summ + "' WHERE cardnumber='" + cardNumber + "'";
            lock.writeLock().lock();
            stmt.executeUpdate(sql);
            lock.writeLock().unlock();
        } finally {
            closeAll(rs, stmt, conn);
        }
    }

    public void transfer(String owner, String recipient, Double summ) throws SQLException {
        changeAccount(recipient, summ);
        changeAccount(owner, summ * -1);
    }

    public String getCardnumberBySessionId(String jsessionid) throws SQLException {
        ResultSet rs = null;
        Statement stmt = null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT cardnumber FROM users WHERE session='" + jsessionid + "'";
            lock.readLock().lock();
            rs = stmt.executeQuery(sql);
            lock.readLock().unlock();
            if (rs.next()) {
                return rs.getString("cardnumber");
            }
            return null;
        } finally {
            closeAll(rs, stmt, conn);
        }
    }

    public boolean accountExists(String cardNumber) throws SQLException {
        ResultSet rs = null;
        Statement stmt = null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT cardnumber FROM users WHERE cardnumber='" + cardNumber + "'";
            lock.readLock().lock();
            rs = stmt.executeQuery(sql);
            lock.readLock().unlock();
            if (rs.next()) {
                return true;
            }
            return false;
        } finally {
            closeAll(rs, stmt, conn);
        }
    }

    public String getRandomCardnumber(String jsessionid) throws SQLException {
        ResultSet rs = null;
        Statement stmt = null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql;
            // TODO ORDER BY RAND() to slow
            sql = "SELECT cardnumber FROM users WHERE session<>'" + jsessionid + "' AND username<>'admin' ORDER BY RAND() LIMIT 1";
            lock.readLock().lock();
            rs = stmt.executeQuery(sql);
            lock.readLock().unlock();
            if (rs.next()) {
                return rs.getString("cardnumber");
            }
            return null;
        } finally {
            closeAll(rs, stmt, conn);
        }
    }
}
