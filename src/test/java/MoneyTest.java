import beans.User;
import db.MysqlController;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.SQLException;

/**
 * Created by JCD on 20.04.2015.
 */
public class MoneyTest extends Assert {

    @Before
    public void setUpMoney() throws SQLException {
        MysqlController mysql = new MysqlController();
        mysql.changeAccount("8888 8888 8888 8888", 100.0);
        User user = mysql.getUser("test", MysqlController.encryptPassword("test"));
        BigDecimal res = user.account;
        assertTrue(res.compareTo(new BigDecimal("100.0")) == 0);
    }

    @Test
    public void testMoney() throws SQLException {
        MysqlController mysql = new MysqlController();
        mysql.changeAccount("8888 8888 8888 8888", -50.0);
        User user = mysql.getUser("test", MysqlController.encryptPassword("test"));
        assertTrue(user.account.compareTo(new BigDecimal("50.0")) == 0);
    }

    @After
    public void tearDownMoney() throws SQLException {
        MysqlController mysql = new MysqlController();
        mysql.transfer("8888 8888 8888 8888", "1383 3230 3707 7826", 50.0);
        User user = mysql.getUser("test", MysqlController.encryptPassword("test"));
        BigDecimal res = user.account;
        assertTrue(res.compareTo(new BigDecimal("0")) == 0);
    }
}
