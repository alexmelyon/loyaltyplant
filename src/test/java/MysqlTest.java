import beans.User;
import db.MysqlController;
import org.junit.*;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by JCD on 20.04.2015.
 */
public class MysqlTest extends Assert {
    @Test
    public void testLogin() throws SQLException {
        String username = "admin";
        String password = MysqlController.encryptPassword("admin");
        String session = "123";
        boolean res = new MysqlController().logIn(username, password, session);
        assertTrue(res);
    }

    @Test
    public void testLogout() throws SQLException {
        assertTrue(new MysqlController().setSession("admin", ""));
    }

    @Test
    @Ignore
    public void testCreateUser() throws SQLException {
        assertTrue(new MysqlController().createUser("test", "test", "8888 8888 8888 8888"));
    }

    @Test
    @Ignore
    public void testAddMoney() throws SQLException {
        MysqlController mysql = new MysqlController();
        mysql.changeAccount("test", 100.0);
        User user = mysql.getUser("test", MysqlController.encryptPassword("test"));
        assertTrue(user.account.compareTo(new BigDecimal("100.0")) == 0);
    }

    @Test
    @Ignore
    public void testTakeMoney() throws SQLException {
        MysqlController mysql = new MysqlController();
        mysql.changeAccount("test", 50.0);
        User user = mysql.getUser("test", MysqlController.encryptPassword("test"));
        assertTrue(user.account.compareTo(new BigDecimal("50.0")) == 0);
    }

    @Test
    @Ignore
    public void testTransferMoney() throws SQLException {
        MysqlController mysql = new MysqlController();
        mysql.transfer("8888 8888 8888 8888", "1383 3230 3707 7826", 50.0);
        User user = mysql.getUser("test", MysqlController.encryptPassword("test"));
        assertTrue(user.account.compareTo(new BigDecimal("0")) == 0);
    }

}
